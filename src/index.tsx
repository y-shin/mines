import React from 'react';
import Main from './main/Main';
import History from './history/History';
import { Provider, useDispatch } from 'react-redux';
import { setupStore } from './redux/store';

import { View, Text, StyleSheet, TouchableHighlight, AsyncStorage } from 'react-native';
import minesModule, { HistorySource } from './redux/slice';

type Display = 'main' | 'history';

const styles = StyleSheet.create({
  tab: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

interface TabProps {
  setDisplay: (d: Display) => void;
}
function Tab(props: TabProps) {
  // react-navigation使えない   https://github.com/microsoft/react-native-windows/issues/3884
  const { setDisplay } = props;
  return (
    <View style={styles.tab}>
      <TouchableHighlight
        onPress={() => {
          setDisplay('main');
        }}
      >
        <Text>Main</Text>
      </TouchableHighlight>
      <TouchableHighlight
        onPress={() => {
          setDisplay('history');
        }}
      >
        <Text>History</Text>
      </TouchableHighlight>
    </View>
  );
}

function Page() {
  const [display, setDisplay] = React.useState<Display>('main');

  if (display === 'main') {
    return (
      <View>
        <Tab setDisplay={setDisplay} />
        <Main />
      </View>
    );
  } else {
    return (
      <View>
        <Tab setDisplay={setDisplay} />
        <History />
      </View>
    );
  }
}

function LoadFile() {
  const dispatch = useDispatch();
  const load = async (_dispatch: any) => {
    const value = await AsyncStorage.getItem('histories');
    if (value) {
      const histories = JSON.parse(value) as HistorySource[];
      _dispatch(minesModule.actions.setHistories(histories));
    }
  };
  React.useEffect(() => {
    load(dispatch);
  }, [dispatch]);

  return <></>;
}

const store = setupStore();
export default function () {
  return (
    <Provider store={store}>
      <LoadFile />
      <Page />
    </Provider>
  );
}
