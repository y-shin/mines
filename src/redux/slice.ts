import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface Level {
  numRows: number;
  numColumns: number;
  numBombs: number;
  name: string;
}
export type GameState = 'preparation' | 'in-game' | 'failure' | 'success';
export interface HistorySource {
  gameId: string;
  levelName: string;
  date: string;
  succeed: boolean;
}
export interface MinesState {
  levels: Level[];
  gameState: GameState;
  histories: HistorySource[];
}

const minesInitialState: MinesState = {
  levels: [
    {
      numRows: 8,
      numColumns: 8,
      numBombs: 2,
      name: 'デバッグ',
    },
    {
      numRows: 9,
      numColumns: 9,
      numBombs: 10,
      name: '初級',
    },
    {
      numRows: 16,
      numColumns: 16,
      numBombs: 40,
      name: '中級',
    },
    {
      numRows: 16,
      numColumns: 30,
      numBombs: 99,
      name: '上級',
    },
  ],
  gameState: 'preparation',
  histories: [],
};
const minesModule = createSlice({
  name: 'mines',
  initialState: minesInitialState,
  reducers: {
    setLevels: (state, action: PayloadAction<Level[]>) => {
      state.levels = action.payload;
    },
    setGameState: (state, action: PayloadAction<GameState>) => {
      state.gameState = action.payload;
    },
    addHistory: (state, action: PayloadAction<HistorySource>) => {
      state.histories.unshift(action.payload);
    },
    setHistories: (state, action: PayloadAction<HistorySource[]>) => {
      state.histories = action.payload;
    },
  },
});

export default minesModule;
