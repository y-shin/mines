import { combineReducers, getDefaultMiddleware, configureStore } from '@reduxjs/toolkit';
import minesModule, { MinesState } from './slice';

const rootReducer = combineReducers({
  mines: minesModule.reducer,
});
export const setupStore = () => {
  const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware(),
  });
  return store;
};

export interface State {
  mines: MinesState;
}
