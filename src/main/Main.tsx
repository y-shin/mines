import React from 'react';
import MineTable from './MineTable';
import { View, Text, Button, StyleSheet, AsyncStorage } from 'react-native';
import minesModule, { Level } from '../redux/slice';
import { State } from '../redux/store';
import { useSelector, useDispatch } from 'react-redux';

function makeSeed(): string {
  let result = '';
  const characters = 'abcdefghijklmnopqrstuvwxyz';
  const charactersLength = characters.length;
  for (let i = 0; i < 10; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const styles = StyleSheet.create({
  gameState: {
    display: 'flex',
    justifyContent: 'center',
  },
  failure: {
    backgroundColor: '#ee8888',
  },
  success: {
    backgroundColor: '#88ee88',
  },
});
function GameStateView() {
  const gameState = useSelector((state: State) => state.mines.gameState);
  if (gameState === 'failure') {
    return (
      <View style={styles.failure}>
        <Text>FAILURE!</Text>
      </View>
    );
  } else if (gameState === 'success') {
    return (
      <View style={styles.success}>
        <Text>SUCCESS!</Text>
      </View>
    );
  } else {
    return (
      <View>
        <Text>&nbsp;</Text>
      </View>
    );
  }
}
function Main() {
  const dispatch = useDispatch();
  const [level, setLevel] = React.useState<Level>({ numBombs: 1, numColumns: 1, numRows: 1, name: 'null' });
  const [bombSeed, setBombSeed] = React.useState<string>('');
  const levels = useSelector((state: State) => state.mines.levels);
  const gameState = useSelector((state: State) => state.mines.gameState);
  const histories = useSelector((state: State) => state.mines.histories);

  const onFailure = async () => {
    const history = {
      gameId: `${level.numRows}-${level.numColumns}-${level.numBombs}-${bombSeed}`,
      levelName: level.name,
      date: new Date().toLocaleString(),
      succeed: false,
    };
    const newHistories = JSON.parse(JSON.stringify(histories));
    newHistories.unshift(history);
    await AsyncStorage.setItem('histories', JSON.stringify(newHistories));
    dispatch(minesModule.actions.addHistory(history));
    dispatch(minesModule.actions.setGameState('failure'));
  };
  const onSuccess = async () => {
    const history = {
      gameId: `${level.numRows}-${level.numColumns}-${level.numBombs}-${bombSeed}`,
      levelName: level.name,
      date: new Date().toLocaleString(),
      succeed: true,
    };
    const newHistories = JSON.parse(JSON.stringify(histories));
    newHistories.unshift(history);
    await AsyncStorage.setItem('histories', JSON.stringify(newHistories));
    dispatch(minesModule.actions.addHistory(history));
    dispatch(minesModule.actions.setGameState('success'));
  };
  const startGame = React.useCallback(
    (lvl: Level) => {
      setLevel(lvl);
      setBombSeed(makeSeed());
      dispatch(minesModule.actions.setGameState('in-game'));
    },
    [setLevel, setBombSeed, dispatch],
  );
  const levelSelects = React.useMemo(() => {
    return levels.map(lvl => {
      return <Button key={lvl.name} onPress={() => startGame(lvl)} title={`Start: ${lvl.name}`} color="#8888ee" />;
    });
  }, [levels, startGame]);

  return (
    <View>
      {levelSelects}
      <GameStateView />
      {gameState === 'preparation' ? (
        <></>
      ) : (
        <MineTable
          onFailure={onFailure}
          onSuccess={onSuccess}
          numRows={level.numRows}
          numColumns={level.numColumns}
          numBombs={level.numBombs}
          bombSeed={bombSeed}
        />
      )}
    </View>
  );
}

export default Main;
