import React from 'react';
import { Md5 } from 'ts-md5/dist/md5';
import { State } from '../redux/store';
import { useSelector } from 'react-redux';
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native';
const styles = StyleSheet.create({
  point: {
    display: 'flex',
    width: 25,
    height: 25,
    borderColor: '#cccccc',
    borderWidth: 1,
    margin: 'auto',
  },
  pointClosed: {
    backgroundColor: '#aaaaaa',
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});

interface PointSource {
  bomb: boolean;
  open: boolean;
  surroundings: number;
}
function searchOpenPoints(
  table: PointSource[][],
  rowId: number,
  colId: number,
  numRows: number,
  numColumns: number,
  points: string[],
) {
  // 対象となるポイントがsurrundings=0の場合、周囲も開ける。それを再帰的に実行する。
  if (rowId >= 0 && rowId < numRows && colId >= 0 && colId < numColumns) {
    const pointId = `${rowId}-${colId}`;

    // 効率悪いかも
    if (!points.includes(pointId)) {
      points.push(pointId);
      if (table[rowId][colId].surroundings === 0) {
        searchOpenPoints(table, rowId - 1, colId - 1, numRows, numColumns, points);
        searchOpenPoints(table, rowId - 1, colId, numRows, numColumns, points);
        searchOpenPoints(table, rowId - 1, colId + 1, numRows, numColumns, points);
        searchOpenPoints(table, rowId, colId - 1, numRows, numColumns, points);
        searchOpenPoints(table, rowId, colId + 1, numRows, numColumns, points);
        searchOpenPoints(table, rowId + 1, colId - 1, numRows, numColumns, points);
        searchOpenPoints(table, rowId + 1, colId, numRows, numColumns, points);
        searchOpenPoints(table, rowId + 1, colId + 1, numRows, numColumns, points);
      }
    }
  }
}
function openPoint(table: PointSource[][], rowId: number, colId: number) {
  const points: string[] = [];
  const numRows = table.length;
  const numColumns = table[0].length;
  searchOpenPoints(table, rowId, colId, numRows, numColumns, points);
  points.forEach(pointId => {
    const pointPair = pointId.split('-');
    const openRowId = parseInt(pointPair[0], 10) as number;
    const openColumnId = parseInt(pointPair[1], 10) as number;
    table[openRowId][openColumnId].open = true;
  });
}

interface PointProps {
  pointSource: PointSource;
  rowId: number;
  colId: number;
  onPressPoint: (point: PointSource, rowId: number, colId: number) => void;
}
function Point(props: PointProps) {
  if (props.pointSource.open) {
    if (props.pointSource.bomb) {
      return (
        <View style={styles.point}>
          <Text>💣</Text>
        </View>
      );
    } else if (props.pointSource.surroundings === 0) {
      return <View style={styles.point} />;
    } else {
      return (
        <View style={styles.point}>
          <Text>{props.pointSource.surroundings}</Text>
        </View>
      );
    }
  } else {
    return (
      <TouchableHighlight
        onPress={() => {
          props.onPressPoint(props.pointSource, props.rowId, props.colId);
        }}
      >
        <View style={[styles.point, styles.pointClosed]} />
      </TouchableHighlight>
    );
  }
}

interface RowProps {
  rowId: number;
  pointSources: PointSource[];
  onPressPoint: (point: PointSource, rowId: number, colId: number) => void;
}
function Row(props: RowProps) {
  // as any しないと謎のエラー https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20356
  const points = props.pointSources.map((pointSource, index) => {
    return (
      <Point
        key={`${props.rowId}-${index}`}
        pointSource={pointSource}
        rowId={props.rowId}
        colId={index}
        onPressPoint={props.onPressPoint}
      />
    );
  }) as any; // eslint-disable-line

  return <View style={styles.row}>{points}</View>;
}

interface MineTableProps {
  pointSourceTable: PointSource[][];
  onPressPoint: (point: PointSource, rowId: number, colId: number) => void;
}
function MineTable(props: MineTableProps) {
  const rows = props.pointSourceTable.map((pointSources: PointSource[], index: number) => {
    return <Row key={index} rowId={index} pointSources={pointSources} onPressPoint={props.onPressPoint} />;
  });
  return <View>{rows}</View>;
}

interface BombShuffle {
  rowId: number;
  colId: number;
  rand: string;
}
interface MineTableContainerProps {
  onFailure: () => void;
  onSuccess: () => void;
  numRows: number;
  numColumns: number;
  numBombs: number;
  bombSeed: string;
}
function MineTableContainer(props: MineTableContainerProps) {
  const [table, setTable] = React.useState<PointSource[][]>([]);
  const { onFailure, onSuccess, numRows, numColumns, numBombs, bombSeed } = props;
  const gameState = useSelector((state: State) => state.mines.gameState);

  React.useEffect(() => {
    const initTable: PointSource[][] = [];

    // 地雷の位置を決定 (seed,numRows,numColumns,numBombsが同じであれば同じ位置になる)
    const bombTable: BombShuffle[] = [];
    for (let r = 0; r < numRows; r++) {
      for (let c = 0; c < numColumns; c++) {
        const rand = Md5.hashStr(`${bombSeed}-${r}-${c}`) as string;
        bombTable.push({
          rowId: r,
          colId: c,
          rand,
        });
      }
    }
    bombTable.sort((a: BombShuffle, b: BombShuffle) => {
      if (a.rand < b.rand) {
        return -1;
      } else if (a.rand > b.rand) {
        return 1;
      } else {
        return 0;
      }
    });
    const bombPlaces: string[] = bombTable.slice(0, numBombs).map((bs: BombShuffle) => {
      return `${bs.rowId}-${bs.colId}`;
    });

    // 初期設定・地雷の配置
    for (let r = 0; r < numRows; r++) {
      const row: PointSource[] = [];
      for (let c = 0; c < numColumns; c++) {
        row[c] = { bomb: bombPlaces.includes(`${r}-${c}`), open: false, surroundings: 0 };
      }
      initTable[r] = row;
    }

    // surroundingsを付与
    const existsBomb = (rr: number, cc: number) => {
      if (rr >= 0 && rr < numRows && cc >= 0 && cc < numColumns) {
        if (initTable[rr][cc].bomb) {
          return 1;
        }
      }
      return 0;
    };
    for (let r = 0; r < numRows; r++) {
      for (let c = 0; c < numColumns; c++) {
        initTable[r][c].surroundings =
          existsBomb(r - 1, c - 1) +
          existsBomb(r - 1, c) +
          existsBomb(r - 1, c + 1) +
          existsBomb(r, c - 1) +
          existsBomb(r, c + 1) +
          existsBomb(r + 1, c - 1) +
          existsBomb(r + 1, c) +
          existsBomb(r + 1, c + 1);
      }
    }

    setTable(initTable);
  }, [bombSeed, numBombs, numColumns, numRows]);

  const onPressPoint = React.useCallback(
    (point: PointSource, rowId: number, colId: number) => {
      if (!point.open && gameState === 'in-game') {
        const newTable: PointSource[][] = JSON.parse(JSON.stringify(table)); // 効率悪いかも
        if (point.bomb) {
          onFailure();
        }

        openPoint(newTable, rowId, colId);

        if (!point.bomb) {
          // 効率悪いかも
          let closedPoints = 0;
          newTable.forEach(row => {
            row.forEach(pt => {
              if (!pt.open) {
                closedPoints++;
              }
            });
          });
          if (closedPoints === numBombs) {
            onSuccess();
          }
        }

        setTable(newTable);
      }
    },
    [setTable, table, onFailure, onSuccess, numBombs, gameState],
  );
  return <MineTable pointSourceTable={table} onPressPoint={onPressPoint} />;
}

export default MineTableContainer;
