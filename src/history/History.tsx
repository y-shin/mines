import React from 'react';
import { State } from '../redux/store';
import { useSelector } from 'react-redux';
import { HistorySource } from 'src/redux/slice';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  historyPanel: {
    margin: 10,
  },
  hr: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 1,
  },
  gameId: {
    color: '#888',
  },
});

interface HistoryPanelProps {
  historySource: HistorySource;
}

function HistoryPanel(props: HistoryPanelProps) {
  const { historySource } = props;
  return (
    <View style={styles.historyPanel}>
      <Text>
        <Text></Text>
        {historySource.succeed ? '✅' : '❌'} {historySource.levelName} - {historySource.date}
      </Text>
      <Text style={styles.gameId}>{historySource.gameId}</Text>
    </View>
  );
}
function History() {
  const histories = useSelector((state: State) => state.mines.histories);

  return (
    <View>
      {histories.map(h => (
        <View key={h.gameId}>
          <HistoryPanel historySource={h} />
          <View style={styles.hr} />
        </View>
      ))}
    </View>
  );
}
export default History;
