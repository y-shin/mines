#pragma once

#include "App.xaml.g.h"

namespace winrt::Mines::implementation
{
    struct App : AppT<App>
    {
        App() noexcept;
    };
} // namespace winrt::Mines::implementation


