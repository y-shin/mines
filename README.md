# AsyncStorage

## react-native-community

- react-native-community の AsyncStorage を使うべきと警告が出るのだが、ドキュメント通りにしてもビルドエラーが起きて使えない
  - https://github.com/react-native-community/async-storage/blob/master/docs/Linking.md
- とりあえず、react-native の AsyncStorage を使う

## ファイル保存先

- C:\Users\<username>\AppData\Local\Packages\<appid>\LocalState\react-native

# 配布・インストール

## アプリ配布

- Visual Studio から Mines.sln を開く
- プロジェクト > 公開 > アプリパッケージの作成
- サイドローディング > 証明書作成
- x86 Release で作成
- Mines_1.0.0.0_Test を配布

## インストール

- 証明書を「信頼されたルート証明書」としてインポート
  - Mines_1.0.0.0_x86.cer ダブルクリック
- PowerShell から ps1 実行
  - `PowerShell -ExecutionPolicy RemoteSigned .\Install.ps1`
- 「現在のユーザーが、このアプリのパッケージ化されていないバージョンを既にインストールしています。これをパッケージ化されたバージョンに置き換えることはできません」などのエラーが出る
  - PoerShell を管理者として実行
  - `Get-appxpackage -allusers -name <競合するアプリID> | Remove-AppxPackage`
